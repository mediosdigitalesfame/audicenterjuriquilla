<!DOCTYPE html>
<html class="no-js">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Audi Center Juriquilla</title>
        <meta name="description" content="Audi Center Juriquilla, Querétaro. Liderazgo por la Tecnología. Concesionaria de autos Audi en Querétaro, México.">
        <meta name="keywords" content="Audi Queretato, Audi Center, Audi Juriquilla, Audi Center Juriquilla, Audi Center Juriquilla Queretaro, Audi Queretaro, A1, A4, A5, A6, A7, A8, Q3, Q5, Q7, TT, R8, e-tron, Queretaro, Qro, Qro-Qro, Autos de lujo Queretaro, Audi center Queretaro, Audi Mexico, Liderazgo en Tecnologia, Audi Juriquilla, Queretaro Audi, Gama Alta, Autos de Lujo, Lujo, deportivos, ecologicos, rendimiento, mejor calidad, lo nuevo en queretaro, queretaro, Mexico.">
        <meta name="author" content="Corporativo grupo FAME División automotriz">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="css/ionicons.min.css">
        <!-- animate css -->
        <link rel="stylesheet" href="css/animate.css">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="css/slider.css">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.css">
        <link rel="stylesheet" href="css/jquery.fancybox.css">
        <!-- template main css file -->
        <link rel="stylesheet" href="css/main.css">
        <!-- responsive css -->
        <link rel="stylesheet" href="css/responsive.css">
        <link rel="stylesheet" href="css/formularios.css">
        <link rel="stylesheet" href="css/style.css">
      
        
        <!-- Template Javascript Files
        ================================================== -->
        <!-- modernizr js -->
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <!-- jquery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- owl carouserl js -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- bootstrap js -->

        <script src="js/bootstrap.min.js"></script>
        <!-- wow js -->
        <script src="js/wow.min.js"></script>
        <!-- slider js -->
        <script src="js/slider.js"></script>
        <script src="js/jquery.fancybox.js"></script>
        <!-- template main js -->
        <script src="js/main.js"></script>
    </head>
    <body>
        <!--
        ==================================================
        Header Section Start
        ================================================== -->
        <header id="top-bar" class="navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <!-- /responsive nav button -->
                    
                    <!-- logo -->
                    <div class="navbar-brand">
                        <a href="index.html" >
                            <img src="images/logo.png" alt="">
                        </a>
                    </div>
                    <!-- /logo -->
                </div>
                <!-- main menu -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <div class="main-menu">
                        <ul class="nav navbar-nav navbar-right">
                             <li>
                                <a href="index.html" >HOME</a>
                            </li>
                        
                            <li><a href="contact.php">CONTACTO</a></li>
                            <li><a href="q5.php">Q5</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- /main nav -->
            </div>
        </header>
        
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-84995808-1', 'auto');
  ga('send', 'pageview');

</script>

<!--
        ================================================== 
            Contact Section Start
        ================================================== -->
        <section id="contact-section">
            <div class="container">
                <div class="row">
                <div class="col-md-12"></div><br><br><br>
                    <div class="col-md-6">
                    <h2 class="subtitle  wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".3s">Ponte en Contacto</h2><br><br><br>
                        <div class="block">
<?php
	if (isset($_POST['boton'])) {
        if($_POST['nombre'] == '') {
        	$errors[1] = '<span class="error">Ingrese su nombre</span>';
        } else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
        	$errors[2] = '<span class="error">Ingrese un email correcto</span>';
        } else if($_POST['telefono'] == '') {
        	$errors[3] = '<span class="error">Ingrese un teléfono</span>';
        } else if($_POST['mensaje'] == '') {
        	$errors[4] = '<span class="error">Ingrese un mensaje</span>';
        } else {
        	$dest = "asesoronline4@contactcenterfame.com";

            // gerencia@audicenterjuriquilla.com, ventas@audicenterjuriquilla.com" . ','. "formas@grupofame.com

            $nombre = $_POST['nombre'];
            $email = $_POST['email'];
            $telefono = $_POST['telefono'];
			$asunto_cte = "Bienvenido a Audi Center Juriquilla";
			$asunto = "Audi Center Juriquilla";
            $cuerpo = $_POST['mensaje'];
			$cuerpo_mensaje .= $nombre . "<br>" . "Mensaje: ". $cuerpo . "<br>" . "Mi correo es: " . $_POST['email'] . "<br>" . "Mi teléfono es: " . $_POST['telefono'];
			$cuerpo_cte = '
			<html>
			<head>
			  <title>Mail from '. $nombre .'</title>
			</head>
			<body>
			  <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Correo:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
				</tr>
			  </table>
			</body>
			</html>
			';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: ' . $email . "\r\n";
            
            if(mail($dest,$asunto,$cuerpo_mensaje,$headers)){
            	$result = '<div class="result_ok">Email enviado correctamente, en breve nos comunicaremos contigo. Gracias por tu preferencia. "Piensa en Auto, Piensa en FAME"</div>';
				mail($email,$asunto_cte,$cuerpo_cte,$headers);
                $_POST['nombre'] = '';
                $_POST['email'] = '';
                $_POST['telefono'] = '';
                $_POST['mensaje'] = '';
            } else {
            	$result = '<div class="result_fail">Hubo un error al enviar el mensaje</div>';
            }
        }
    }
?>
	<meta charset="utf-8">
    
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
    <link rel='stylesheet'| href='css/formularios.css'>
    
	<form id="contact-form" class="contact-work-form2" method='post' action=''>
    	<div class="text-input">
        	<div class="float-input">
            <input name='nombre' id="nombre" placeholder="Nombre*" type='text' class='nombre' value='<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>'> 
        <?php if(isset($errors)){ echo $errors[1]; } ?><span><i class="fa fa-user"></i></span></div>
        
        <div class="float-input2"><input name='email' placeholder="E-mail*" type='text' class='email' value='<?php if(isset($_POST['email'])){ echo $_POST['email']; } ?>'>
        <?php if(isset($errors)){ echo $errors[2]; } ?><span><i class="fa fa-envelope"></i></span></div>
        </div>
        
        <div class="text-input">
        <div class="float-input">
        <input name='telefono' id="telefono" placeholder="Teléfono*" type='tel' class='telefono' value='<?php if(isset($_POST['telefono'])){ echo $_POST['telefono']; } ?>'>
        <?php if(isset($errors)){ echo $errors[3]; } ?><span><i class="fa fa-phone"></i></span>
        </div>
        </div>
        
        <div class="textarea-input"><textarea name='mensaje' placeholder="Mensaje*" rows='5' class='mensaje'><?php if(isset($_POST['mensaje'])){ echo $_POST['mensaje']; } ?></textarea>
        <?php if(isset($errors)){ echo $errors[4]; } ?><span><i class="fa fa-comment"></i></span>
        </div>
        <div><input name='boton' type='submit' value='Enviar' class='boton'></div>
        <br><br><br>
<?php if(isset($result)) { echo $result; } ?>
    </form> 
                       

                        </div>
                    </div>
                    
                      <div class="col-md-6">
                         <div class="map-area">
                            <h2 class="subtitle  wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".3s">Encuentranos</h2>
                            <p class="subtitle-des wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".5s">
                                Concesionario Audi Center Juriquilla 
                                
                            </p>
                            <div class="single-project-content"><div class="map">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14932.289038662908!2d-100.397271!3d20.666639!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x92e7646c22fa58ad!2sAudi+Center+Juriquilla!5e0!3m2!1ses!2smx!4v1475003273915" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                
                            </div></div>
                        </div>
                    </div>
                
                <div class="row address-details">
                    <div class="col-md-6">
                        <div class="address wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".3s">
                            <i class="ion-ios-location-outline"></i>
                            <h5> Anillo Vial Fray Junipero Serra #17512<br>Querétaro,Querétaro. México</h5>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="address wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".3s">
                            <i class="ion-ios-email-outline"></i>
                            <h5>atencionaclientes@audicenterjuriquilla.com<br>
                             Teléfono: (442) 402 1710</h5>
                        </div>
                    </div>
                    
                    
                   </div>  
                   
                </div>
            </div>
        </section>






     
        <!--  ==================================================
            Footer Section Start
            ================================================== -->
          <footer id="footer">
                <div class="container">
                    <div class="col-md-8">
                        <p class="copyright"><a href="aviso.html"><strong>Aviso de privacidad</strong></a><span></span><strong> · Audi Center Juriquilla · 2017</strong></p>
                    </div>
                    <div class="col-md-4">
                        <!-- Social Media -->
                        <ul class="social">
                            <li>
                                <a href="https://www.facebook.com/AUDIFAME/?ref=br_rs" class="Facebook">
                                    <i class="ion-social-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/AudiFame?lang=es" class="Twitter">
                                    <i class="ion-social-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/user/GrupoFameAutos" class="Youtube">
                                    <i class="ion-social-youtube"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer> <!-- /#footer -->
            
            </html>


